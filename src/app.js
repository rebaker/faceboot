require('./assets/styles/index.css');
require('angular');
require('angular-ui-router');

var app = angular.module('app', ['ui.router'])

app.config(function($stateProvider, $locationProvider, $urlRouterProvider) {
	$locationProvider.html5Mode(true); //gets rid of # in url
	$urlRouterProvider.otherwise('/');
})

const context = require.context('.', true, /\.(component|service|controller)\.js$/);
context.keys().forEach(context);

console.dir(angular);