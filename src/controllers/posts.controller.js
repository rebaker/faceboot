angular.module('app')
.controller('postCtrl', ['$scope', 'postService', 'userService',
	function($scope, postService, userService) {
		vm = this;

		var posts = undefined;

		vm.getPosts = function() {
			if(!posts) {
				postService.getPosts().then(function(data) {
					posts = data;
				});		
				return posts;	
			}
			else {
				return posts;
			}
		}


		vm.addComment = function(index, text, form) {
			com = new Object();
			com.username = userService.getCurrentUser();
			com.content = text;
			com.published_at = new Date().toJSON();
			postService.addComment(index, com);

			form.newComment = "";
		}


		vm.changeLikes = function(index, event) {
			postService.changeLike(index, posts);

			event.currentTarget.classList.toggle("btn--active");
			event.currentTarget.classList.toggle("btn--hover-red");
		}
}])