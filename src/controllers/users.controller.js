angular.module('app')
.controller('userCtrl', ['$scope', '$state', '$stateParams', 'userService', 
	function($scope, $state, $stateParams, userService) {
		var vm = this;

		vm.getProfile = function() {
			return $stateParams.username;
		}

		var users = undefined;
		vm.getUsers = function() {
			if(!users) {
				userService.getUsers().then(function(data) {
					users = data;
				});		
				return users;	
			}
			else {
				return users;
			}
		}
		vm.getUsers();

		vm.getUserHobbies = function() {
			var name = vm.getProfile();
			var user = userService.getUserData(name);
			if(user) {
				if(user.hobbies) {
					return user.hobbies;
				}
			}
			return [];
		}

		vm.getUserImage = function(name) {
			var user = userService.getUserData(name);
			if(user) {
				if(user.image) {
					return user.image;
				}
			}
			return './assets/stock_profile.jpg'
		}

		vm.getCurrentUser = function() {
			return userService.getCurrentUser();
		}

		vm.showUserInfo = function(name) {
			$state.go('profile', {username: name });
		}
}]);