angular.module('app')
.controller('profileCtrl', ['$scope', '$state', '$stateParams', 'userService', 
	function($scope, $state, $stateParams, userService) {
		var vm = this;

		vm.getProfile = function() {
			return $stateParams.username;
		}

		vm.getUserHobbies = function() {
			var name = vm.getProfile();
			var user = userService.getUserData(name);
			if(user) {
				if(user.hobbies) {
					return user.hobbies;
				}
			}
			return [];
		}
}]);