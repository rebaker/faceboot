angular.module('app')
.component('navbar',{
	controller: 'userCtrl',
	template: 
	`<header class="page-header">
		<!--navbar block-->
		<div class="navbar">
			<a class="logo" href="/"></a>
			<ul class="nav-h">
				<li>
					<div class="btn profile--hover-bg profile-color">
						<user-info info="$ctrl.getCurrentUser()"></user-info>
					</div>
				</li>
				<li>
					<a class="btn" href="http://www.google.com">
						<i class="fa fa-sign-out" aria-hidden="true"></i>
						<span>Log Out</span>
					</a>
				</li>
			</ul>
		</div>
	</header>`
})