angular.module('app')
.component('postInput',{
	bindings: {
		index: '<',
	},
	require: {
		parentCtrl: '^content'
	},
	template: `
	<form class="p-input" name="addInput" ng-submit="$ctrl.parentCtrl.addComment($ctrl.index, addInput.newComment, addInput)">
		<input type="textarea" class="p-input__ta" ng-model="addInput.newComment" placeholder="Add a comment..."></input>
		<input type="submit" class="btn btn-primary f-right margin-8-v postSubmit" value="POST"></input>
	</form>`

})