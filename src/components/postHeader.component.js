angular.module('app')
.component('postHeader',{
	bindings: {
		username: '<',
		date: '<'
	},
	template: `
	<div class="post__header flex-between margin-16">
		<user-info class="profile--lg profile--hover-ul" info="$ctrl.username"></user-info>
		<timestamp time="$ctrl.date"></timestamp>
	</div>`
})
              