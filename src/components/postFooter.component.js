angular.module('app')
.component('postFooter',{
	bindings: {
		index: '<',
		likes: '<',
		number: '<'
	},
	template: `
	<div class="flex-between margin-16">
		<like-button class="flex" index="$ctrl.index" likes="$ctrl.likes"></like-button>
		<comment-number class="flex" number="$ctrl.number"></comment-number>
	</div>`
})
              