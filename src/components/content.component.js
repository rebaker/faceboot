angular.module('app')
.component('content',{
	controller: 'postCtrl',
	template: 
	`<div ng-repeat="post in $ctrl.getPosts()">
		<post data="post" index="$index"></post>
	</div>`
})