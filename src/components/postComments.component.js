angular.module('app')
.component('postComments',{
	bindings: {
		index: '<',
		comments: '<'
	},
	template: `
	<ul class="margin-16 top-divider">
		<div class="comments">
			<comment data="com" ng-repeat="com in $ctrl.comments"></comment>
		</div>
		<post-input index="$ctrl.index"></post-input>
	</ul>`
})
              