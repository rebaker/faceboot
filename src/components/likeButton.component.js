angular.module('app')
.component('likeButton',{
	bindings: {
		index: '<',
		likes: '<',
	},
	require: {
		parentCtrl: '^content'
	},
	template: `
	<a class="btn-icon btn--hover-red" href="#"
		ng-click="$ctrl.parentCtrl.changeLikes($ctrl.index, $event)">
		<i class="fa fa-heart fa-lg"></i>
	</a>
	<span>{{$ctrl.likes}}</span>`

})