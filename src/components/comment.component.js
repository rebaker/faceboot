angular.module('app')
.component('comment',{
	bindings: {
		data:'<'
	},
	template: `
	<li class="flex-col">
		<comment-header username="$ctrl.data.username" date="$ctrl.data.published_at"></comment-header>
		<comment-text text="$ctrl.data.content"></comment-text>
	</li>`
})