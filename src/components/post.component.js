angular.module('app')
.component('post',{
	bindings: {
		data:'<',
		index: '<'
	},
	template: `
	<!-- USER POST-->
	<div class="post">
		<post-header username="$ctrl.data.username" date="$ctrl.data.published_at"></post-header>
		
		<post-text text="$ctrl.data.content"></post-text>

		<post-footer index="$ctrl.index" 
					 likes="$ctrl.data.likes"
					 number="$ctrl.data.replies.length">
		</post-footer>

		<post-comments comments="$ctrl.data.replies" index="$ctrl.index"></post-comments>
	</div>`
});