angular.module('app')
.component('timestamp', {
	bindings: {
		time: '<'
	},
	template: `
	<span class="timestamp">
		{{$ctrl.time | date: "MMM dd"}} at {{$ctrl.time | date: "h:mm a"}}
	</span>`
})