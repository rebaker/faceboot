angular.module('app')
.component('commentNumber',{
	bindings: {
		number: '<',
	},
	template: `
	<a class="btn-icon" href="#">
		<i class="fa fa-comments-o fa-lg"></i>
	</a>
    <span>{{$ctrl.number}}</span>`
})
              