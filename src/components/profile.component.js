angular.module('app')
.config(function($stateProvider) {
	$stateProvider
	.state("profile",{
		url: '/profile/:username',
		component: 'profile',
		params: { username: null }
	});
})
.component('profile',{
	controller: 'profileCtrl',
	template: 
		`<navbar></navbar>
		<div class="container">
			<div class="content">
				<div class="user-profile profile--lg">
					<user-info class="profile-header" info="$ctrl.getProfile()"></user-info>
					<div class="margin-16">
						<p>Hobbies</p>
						<div ng-show="!$ctrl.getUserHobbies().length">None listed</div>
						<ul ng-repeat="hobby in $ctrl.getUserHobbies()">
							<li>{{hobby}}</li>
						</ul>			
					</div>	
				</div>
			</div>
			<aside class="aside"></aside>
		</div>
		<my-footer></my-footer>`
})
