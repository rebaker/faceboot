angular.module('app')
.component('userInfo', {
	bindings: {
		info: '<'
	},
	controller: 'userCtrl',
	template: `
	<a class="profile" href="#" ng-click="$ctrl.showUserInfo($ctrl.info)"">
		<img ng-src="{{$ctrl.getUserImage($ctrl.info)}}">
		<span class="userName">{{$ctrl.info}}</span>
	</a>`
})