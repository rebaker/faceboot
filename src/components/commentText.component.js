angular.module('app')
.component('commentText',{
	bindings: {
		text: '<'
	},
	template: `
	<p class="text-md margin-8-top">
		{{$ctrl.text}}
	</p>`
})
              