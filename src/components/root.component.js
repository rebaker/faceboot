angular.module('app')
.config(function($stateProvider) {
	$stateProvider
	.state("base",{
		url: '/',
		component: 'root'
	});
})
.component('root',{
	template: 
		`<navbar></navbar>
		<div class="container">
			<content class="content"></content>
			<aside class="aside"></aside>
		</div>
		<my-footer></my-footer>`,
})
