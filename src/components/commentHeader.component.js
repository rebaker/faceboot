angular.module('app')
.component('commentHeader',{
	bindings: {
		username: '<',
		date: '<'
	},
	template: `
	<div class="flex-between">
		<user-info info="$ctrl.username" class="profile--hover-ul"></user-info>
		<timestamp time="$ctrl.date" class="text-sm"></timestamp>
	</div>`
})
              