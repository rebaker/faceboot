angular.module('app')
.component('postText',{
	bindings: {
		text: '<'
	},
	template: `
	<div class="text-md margin-16">
		{{$ctrl.text}}
	</div>`
})
              