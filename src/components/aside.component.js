angular.module('app')
.component('aside',{
	controller: 'userCtrl',
	template: `
	<div>
		<span>Users</span>
		<div ng-repeat="user in $ctrl.getUsers()">
			<br>
			<user-info info="user.username"></user-info>
		</div>
	</div>`
})