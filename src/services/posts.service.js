angular.module('app')
.service("postService",function($http, $q) {
	var vm = this;

	var posts = undefined;
	var likes = undefined;
	var deferredLikes = $q.defer();

	vm.getPosts = function() {
		if(!posts) {
			var deferredPosts = $q.defer();
			$http({                
				method: 'GET',
                url: 'http://private-36ae7-bootcamp3.apiary-mock.com/questions',
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(
				function(result) {
					posts = result.data;
					likes = Array(result.data.length).fill(false);

					deferredPosts.resolve(posts);
					deferredLikes.resolve(likes);
				}
			);
			posts = deferredPosts.promise;
		}
		return $q.when(posts);
	}

	vm.addComment = function(index, comment) {
		posts[index].replies.push(com);
	};

	vm.changeLike = function(index) {
		likes[index] = !likes[index];
		likes[index] ? posts[index].likes++ : posts[index].likes--;
	};
});
