angular.module('app')
.service("userService",function($http, $q) {
	
    var users = undefined;
    var vm = this;

    vm.getUsers = function() {
        if(!users) {
            var deferredUsers = $q.defer();
            $http({
                method: 'GET',
                url: 'http://private-36ae7-bootcamp3.apiary-mock.com/users',
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(
                function(result) {
                    users = result.data;
                    deferredUsers.resolve(users);
                }
            );
            users = deferredUsers.promise;
        }
        return $q.when(users);
    };

    vm.getUserData = function(name) {
        if(users) {
            if(users.constructor === Array) {
                var user = users.filter(function(user) {
                    return user.username == name;
                })[0];;
                return user;     
            }
        }
        return undefined;
    }

    this.getCurrentUser = function() {
        return "Keegan";		
    }
});