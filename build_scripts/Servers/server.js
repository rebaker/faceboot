const
	path = require('path'),
	express = require('express'),
	favicon = require('serve-favicon'),
	open = require('open'),
	webpack = require('webpack'),
	webpack_config = require('../../webpack.config.dev')

	app = express(),
	port = 3000,
	compiler = webpack(webpack_config);

app.use(favicon(path.join(__dirname,'../../src/assets/favicon.png')));
app.use(require('webpack-dev-middleware')(compiler,{
	publicPath: webpack_config.output.publicPath
}));

app.get('/',(req,res) => res.sendFile(path.join(__dirname,'../../src/index.html')));
app.get('/profile/:username',(req,res) => res.sendFile(path.join(__dirname,'../../src/profile.html')));

app.listen(port, err => err ? console.log(err) : open(`http://localhost:${port}`));